# Apache Kafka JMS Receiver based on Spring Integration #

To be honest, after getting my hands on Apache Kafka JMS Broker I would say that Apache Kafka is something like GIT but amongs the JMS brokers. Very power full, but you need to have perfect knowledge of how it works to use it successfully. Let's summarize the basic features important to know when writing JMS receiver from Apache Kafka:

* Apache Kafka is **stateless** in terms of that Kafka doesn't keep track of which messages have been consumed. This is responsibility of JMS consumers. This simplicity make Kafka very fast.

* JMS consumer of Apache Kafka messages, needs to keep **message offset** to be able to track which messages he consumed. Mentioned offset also gives him opportunity to rewind to particular position and re-read messages again. 

* When writing messages into Kafka, then messages are written into the end of partition. This spares the disk seeking and it is also another performance boost of Apache Kafka.

## Writing JMS consumer of Apache Kafka using Spring Integration ##

If you want to write Apache Kafka JMS receiver using Spring Integration you have two options:

* Using **Inbound Channel Adapter based on high-level** Apache Kafka API.
* Using **Message Driven Adapter based on Apache Kafka's SimpleConsumer**.

And what is the difference? Using *Inbound channel adapter* **you don't have access to used offset into topic's partition**. Offset is managed via Zookeeper, which means that you can't rewind to old position in the topic's partition to re-read messages. If that's OK for you then Inbound channel adapter is your friend.

On the other hand, *Message Driven Adapter* using Kafka's SimpleConsumer gives us **org.springframework.integration.kafka.listener.OffsetManager** to keep track the offset.

In this demo, we will test getting the messages via InboundChannel Adapter. Because most of the time this option is enough.

## High Level API Kafka JMS Messages Receiver ##

To get up to speed about how to start and install Apache Kafka, read my previous tutorial at: https://bitbucket.org/tomask79/apache-kafka-spring-integration

Anyway, as usual we will build simple Spring-Boot based application with integrated Spring Integration dependency. Main class looks like this:

```
@SpringBootApplication
@EnableIntegration
@ImportResource("applicationContext.xml")
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
```
Now Spring Integration part:

```
<int:channel id="inputFromKafka">
    	<int:queue />
	</int:channel>
	
	<bean id="messageProcessor" class="com.example.processor.MessageProcessor" />
 
	<int:service-activator input-channel="inputFromKafka"
    	ref="messageProcessor">
    	<int:poller fixed-delay="10" />
	</int:service-activator>
```
We simply defined here **queue-pollable channel** inputFromKafka, where messages in this channel will be then handed over into com.example.processor.**MessageProcessor**. Polling interval is 10 milliseconds.
In mentioned processor, let's just printout the messages.

```
public class MessageProcessor {
   public void handleMessage(Message<?> message) {
		 System.out.println("**************************************************");
System.out.println("Spring integration received:"+message.getPayload());
System.out.println("**************************************************");
	}
}
```

Very important is to keep in mind that **payload structure of Spring Integration message** converted from Apache Kafka JMS message is the following:

```
Map<String, Map<Integer, List<Object>>>
```

Key of first map is the ID of the topic, Key of second Map is the ID of the topic's partition and List<Object> is the list of payloads.

Now Spring Integration Kafka configuration:

```
<bean id="consumerProperties"
    	class="org.springframework.beans.factory.config.PropertiesFactoryBean">
    	<property name="properties">
        	<props>
            	<prop key="auto.offset.reset">smallest</prop>
            	<prop key="socket.receive.buffer.bytes">10485760</prop> 
            	<prop key="fetch.message.max.bytes">5242880</prop>
            	<prop key="auto.commit.interval.ms">10</prop>
        	</props>
    	</property>
	</bean>
 
 
	<int-kafka:zookeeper-connect id="zookeeperConnect"
    	zk-connect="localhost:2181" zk-connection-timeout="6000"
    	zk-session-timeout="6000" zk-sync-time="2000" />
 
  
	<int-kafka:inbound-channel-adapter
    	kafka-consumer-context-ref="consumerContext" 
    	auto-startup="true"
    	channel="inputFromKafka" 
    	id="kafka-inbound-channel-adapter">
    	<int:poller fixed-delay="10" 
    				time-unit="MILLISECONDS"
        			receive-timeout="0" />
	</int-kafka:inbound-channel-adapter>
	
	<bean id="kafkaStringSerializer" 
          class="org.springframework.integration.kafka.serializer.common.StringDecoder" />
 
	<int-kafka:consumer-context id="consumerContext"
    	consumer-timeout="4000" zookeeper-connect="zookeeperConnect"
    	consumer-properties="consumerProperties">
    	<int-kafka:consumer-configurations>
        	<int-kafka:consumer-configuration
            	group-id="tomask79_consumer" 
            	max-messages="50" 
            	value-decoder="kafkaStringSerializer">
            	<int-kafka:topic id="test_topic" streams="1" />
        	</int-kafka:consumer-configuration>
    	</int-kafka:consumer-configurations>
	</int-kafka:consumer-context>
```
Where you can find inboundchannel-adapter feeding our inputToKafka channel with messages from Apache Kafka. Also check following properties:


```
auto.commit.interval.ms
```
This property says the frequency in ms that the consumer offsets are committed to zookeeper. This helps Kafka in tracking which messages are new or already read.

```
auto.offset.reset
```
This is hint of Kafka what to do when offset in zookeper is out of actual range. 
This settings says where to shift in topic partition to be able to read messages in that case.

Other settings are more or less self-explanatory. Let's build and start our Spring-Boot maven application, run:


```
mvn clean install
```
In the maven root and then to launch:

```
java -jar target/receiver-0.0.1-SNAPSHOT.jar
```

Now let's test our consumer. First we will start **Apache Kafka console producer to send some messages** into topic.

```
kafka-console-producer.sh --broker-list localhost:9092 --topic test_topic
```
This command will start standard command input, where if we type for example **"Hello Spring Integration kafka"** then our message should be displayed also by our demo application like this:

```
**************************************************
Spring integration received: {test_topic={0=[Hello Spring Integration kafka]}}
**************************************************
```

Maybe if I will find some time I'd test Message Driven adapter with lower level Kafka API. But like I said, most of the time high-level Kafka API with Spring Integration is enough. Cheers guys and rulez to Spring Integration!

Regards

Tomas









