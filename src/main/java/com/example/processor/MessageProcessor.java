package com.example.processor;

import org.springframework.messaging.Message;

public class MessageProcessor {
	public void handleMessage(Message<?> message) {
		System.out.println("**************************************************");
		System.out.println("Spring integration received: "+message.getPayload());
		System.out.println("**************************************************");
	}
}
